/**
 * module dependencies for server configuration
 */
const path = require('path');

/**
 * server configurations
 */
const serverConfigs = {
  PRODUCTION: process.env.NODE_ENV === 'production',
  PORT: process.env.PORT || 3001,
  ROOT: path.resolve(__dirname, '..'),
  FRONT_HOST: 'http://localhost:3000',
  BACK_HOST: 'http://localhost:8080',
};

module.exports = serverConfigs;
