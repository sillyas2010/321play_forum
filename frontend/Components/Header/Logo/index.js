import React, { Component } from 'react';
import styles from './styles';
import logo321 from './321-logo.svg';
import { Link } from 'react-router-dom';

const Logo = () => {
  return (
    <div className={styles.logoContainer}>
      <div className={styles.logo}>
        <a href="/">
          <img src={logo321}/>
        </a>
      </div>
      <div className={styles.logoTitle}>
        <Link to="/" className={styles.link}>Forum</Link>
      </div>
    </div>
  );
};

export default Logo;
