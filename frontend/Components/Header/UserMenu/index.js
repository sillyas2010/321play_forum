import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import onClickOutside from 'react-onclickoutside';
import styles from './styles';

import Button from 'Components/Button';

class UserMenu extends Component {
  constructor(props) {
    super(props);
    this.state = { activeSubMenu: false };
    this.toggleSubMenu = this.toggleSubMenu.bind(this);
  }

  handleClickOutside() {
    this.setState({ activeSubMenu: false });
  }

  toggleSubMenu() {
    this.setState((prevState) => {
      return { activeSubMenu: !prevState.activeSubMenu };
    });
  }

  renderSubMenu() {
    const { activeSubMenu } = this.state;
    const {
      signedIn,
      gitHandler,
      signOutAction,
      role,
    } = this.props;

    if (activeSubMenu) {
      return (
        <div className={styles.subMenu}>
          <Button className={styles.subMenuClose} onClick={this.toggleSubMenu} alwaysActive>
            <i className={classnames('fa fa-close')}></i>
          </Button>

          { !signedIn && <a className={styles.signInLink} href={'/login'}>
            <Button className={styles.gitLoginBtn} alwaysActive>
              <span className={styles.btnLabel}>Login</span>
            </Button>
          </a> }

          { signedIn && <span onClick={this.toggleSubMenu}><Link className={styles.subMenuItem} to={`/user/${gitHandler}`}>My Profile</Link></span> }
          { signedIn && role === 'admin' && <span onClick={this.toggleSubMenu}><Link className={styles.subMenuItem} to={'/admin'}>Admin Panel</Link></span> }
          {/* { signedIn && <a className={styles.subMenuItem} href={'#'}>Settings</a> } */}
          { signedIn && (
              <Button className={styles.gitLoginBtn} onClick={() => signOutAction() }>
                <span className={styles.btnLabel}>Sign Out</span>
              </Button>
            )
          }
        </div>
      );
    }

    return null;
  }

  render() {
    const {
      signedIn,
      userName,
      avatar,
    } = this.props;

    if (signedIn) {
      return (
        <div style={{ position: 'relative' }}>
          <div className={styles.container} onClick={this.toggleSubMenu}>
            <img className={styles.userAvatar} src={avatar} alt={`${userName} Avatar`} />
            <span className={styles.title}>{userName}</span>
          </div>
          {this.renderSubMenu()}
        </div>
      );
    }

    return (
      <div className={styles.container}>
        <Button
          alwaysActive
          className={classnames(styles.signInBtn, styles.title)}
          onClick={this.toggleSubMenu}
        >
          Sign Up / Sign In
        </Button>

        {this.renderSubMenu()}
      </div>
    );
  }
}

UserMenu.defaultProps = {
  signedIn: false,
  signOutAction: () => {},
  userName: '',
  gitHandler: '',
  avatar: '',
};

UserMenu.propTypes = {
  signedIn: React.PropTypes.bool.isRequired,
  signOutAction: React.PropTypes.func,
  userName: React.PropTypes.string,
  gitHandler: React.PropTypes.string,
  avatar: React.PropTypes.string,
};

export default onClickOutside(UserMenu);
