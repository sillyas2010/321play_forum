import React from 'react';
import ReactDOM from 'react-dom';
import { Router, HashRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import styles from './styles';
import browserHistory from './history';

// app store
import appStore from './store';

// app views
import AppContainer from './App';
import AdminContainer from './Admin';
import LayoutContainer from '../Containers/LayoutContainer/LayoutContainer';
import Dashboard from '../Views/AdminDashboard';
import ForumFeed from '../Views/ForumFeed';
import SingleDiscussion from '../Views/SingleDiscussion';
import NewDiscussion from '../Views/NewDiscussion';
import UserProfile from '../Views/UserProfile';
import NotFound from '../Views/NotFound';

ReactDOM.render (
  <Provider store={appStore}>
    <HashRouter history={browserHistory}>
      <Switch>
        <LayoutContainer path="/admin/" component={Dashboard} layout={AdminContainer} />
        <LayoutContainer path="/user/:username" component={UserProfile} layout={AppContainer}/>
        <LayoutContainer path="/:forum/new_discussion" component={NewDiscussion} layout={AppContainer}/>
        <LayoutContainer path="/:forum/discussion/:discussion" component={SingleDiscussion} layout={AppContainer}/>
        <LayoutContainer path="/:forum" component={ForumFeed} layout={AppContainer}/>
        <LayoutContainer exact path="/" component={ForumFeed} layout={AppContainer}/>
        <LayoutContainer path="*" component={NotFound} layout={AppContainer}/>
      </Switch>
    </HashRouter>
  </Provider>,
  document.getElementById('root')
);
