import axios from 'utils/Axios';

export const fetchForums = (forum_id) => {
  return axios.get('/api/forum');
};

export const fetchUser = () => {
  return axios.get('/api/users/self');
};
