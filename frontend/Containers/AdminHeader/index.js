import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import appLayout from 'SharedStyles/appLayout';
import styles from './styles';

// components for AdminHeader
import UserMenu from 'Components/Header/UserMenu';
import Logo from 'Components/Header/Logo';
import NavigationBar from 'Components/Header/NavigationBar';
import PlaceholderImage from 'SharedStyles/placeholder.jpg';
import { signOutUser } from '../../App/actions';

class AdminHeader extends Component {
  renderNavLinks() {
    return [
      { name: 'Dashboard', link: '/admin' },
    ];
  }

  render() {
    const {
      authenticated,
      name,
      username,
      role,
      avatarUrl,
    } = this.props.user,
    { signOutUser } = this.props;

    return (
      <div className={classnames(appLayout.constraintWidth)}>
        <div className={styles.headerTop}>
          <Logo />
          Welcome Admin
          <UserMenu
            signedIn={authenticated}
            userName={name || username}
            role={role}
            gitHandler={username}
            avatar={avatarUrl}
            signOutAction={signOutUser}
          />
        </div>
        <NavigationBar
          navigationLinks={this.renderNavLinks()}
        />
      </div>
    );
  }
}

export default connect(
  (state) => { return {
    user: state.user,
    forums: state.app.forums,
  }; },
  (dispatch) => { return {
    signOutUser: () => { dispatch(signOutUser()); },
  }; }
)(AdminHeader);
