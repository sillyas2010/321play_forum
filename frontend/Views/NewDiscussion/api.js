import axios from 'utils/Axios';

export const postDiscussionApi = (discussion) => {
  return axios.post('/api/discussion/newDiscussion', discussion);
};
