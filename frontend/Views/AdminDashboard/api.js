import axios from 'utils/Axios';

export const getAdminDashboardInfoAPI = () => {
  return (axios.get('/api/admin_users/admin_dashboard_info'));
};

export const createForumAPI = (forum_obj) => {
  return (axios.post('/api/forum/create_forum', forum_obj));
};

export const deleteForumAPI = (forum_id) => {
  return (axios.post('/api/forum/delete_forum', { forum_id }));
};
