/**
 * user profile apis
 */

import axios from 'utils/Axios';

export const fetchUserProfileApi = (userSlug) => {
  return axios.get(`/api/users/profile/${userSlug}`);
};
