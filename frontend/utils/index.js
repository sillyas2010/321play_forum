export const userAdapter = function(apiUserObj) {
  if(apiUserObj === null || typeof apiUserObj !== 'object') return {};

  const {
    _id,
    avatar,
    email,
    role_id,
    first_name,
    last_name,
  } = apiUserObj,
  avatarUrl = `/api/public/${_id}/${avatar}`,
  name = `${first_name} ${last_name}`;

  return {
    id: _id,
    _id,
    avatarUrl,
    avatar: avatarUrl,
    email,
    first_name,
    last_name,
    role: role_id.role,
    username: name,
    name,
  };
};