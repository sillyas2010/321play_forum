import axios from 'axios';
const apiClient = axios.create();
apiClient.interceptors.request.use(function (request) {
  const token = localStorage.getItem('token');
  if(token) {
    request.headers.authorization = `Bearer ${token}`;
  }
  return request;
});
apiClient.interceptors.response.use(function (response) {
  return response;
}, function (error) {
  const data = error.response;
  if(data.status === 401 || data.status === 403) {
    localStorage.removeItem('token');
    window.location = '/login';
  }
  return Promise.reject(error);
});

export default apiClient;
