/**
 * module dependencies for routes configuration
 */
const path = require('path');
const express = require('express');
const { createProxyMiddleware } = require('http-proxy-middleware');
const serverConfigs = require('../config/serverConfig');

/**
 * routes configurations
 */
const routesConfig = (app) => {
  // serves static files from public directory
  const publicPath = path.resolve(__dirname, '../public');
  app.use('/forum', express.static(publicPath));

  app.all('/api/*', createProxyMiddleware({ 
    target: serverConfigs.BACK_HOST, 
    changeOrigin: true,
    onProxyReq: function(proxyReq, req, res) {  
      if (req.body) {  
        // Make any needed POST parameter changes
        var body = {};
  
        // URI encode JSON object
        body = Object.keys(req.body)
          .map(function(key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(req.body[key]);
          })
          .join('&');

        delete req.body;
  
        // Update header
        proxyReq.setHeader('content-type', 'application/x-www-form-urlencoded');
        proxyReq.setHeader('content-length', body.length);
  
        // Write out body changes to the proxyReq stream
        proxyReq.write(body);
        proxyReq.end();
      }
    },
  }));

  // all get request will send index.html for react-router
  // to handle the route request
  app.get('/forum', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../public', 'index.html'));
  });

  app.all('/*', createProxyMiddleware({ target: serverConfigs.FRONT_HOST, changeOrigin: true }));
};

module.exports = routesConfig;
