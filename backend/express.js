/**
 * module dependencies for express configuration
 */
const morgan = require('morgan');
const compress = require('compression');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const flash = require('connect-flash');

/**
 * express configuration
 */
const expressConfig = (app, serverConfigs) => {

  // apply gzip compression (should be placed before express.static)
  app.use(compress());

  // log server requests to console
  !serverConfigs.PRODUCTION && app.use(morgan('dev'));

  // get data from html froms
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  // read cookies (should be above session)
  app.use(cookieParser());

  // connect flash for flash messages (should be declared after sessions)
  app.use(flash());

  // apply development environment additionals
  if (!serverConfigs.PRODUCTION) {
    require('./dev')(app);
  }

  // apply route configs
  require('./routes')(app);
};

module.exports = expressConfig;
